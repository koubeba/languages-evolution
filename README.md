# Language evolution

Project for heuristic algorithms course (ALHE) in Warsaw University of Technology.

Authors: Maja Jabłońska, Marta Pacuszka

## Run program

Program can be run in 2 modes:

1. `from-cmd`: Perform a single evolution with the given properties.
    ```
    Usage: run_cmd.py [OPTIONS]

    Options:
    --init-pop [RANDOM|GEO]         Type of algorithm to generate initial
                                    population
    --evaluate [ZERO|BINARY|PROPORTIONAL]
                                    Type of algorithm to calculate fitness
    --select [KEEP_EXISTING|TOURNAMENT]
                                    Type of algorithm to select individuals to
                                    the next generation
    --pop-size INTEGER              Size of initial population
    --pmut-prob FLOAT               Independent probability of one property
                                    mutation
    --pcross-prob FLOAT             Independent probability of crossing a
                                    property between mating individuals
    --mut-prob FLOAT                Independent probability of mutation between
                                    a pair of individuals
    --cross-prob FLOAT              Independent probability of crossing a pair
                                    of individuals
    --max-steps INTEGER             Maximal number of evolution steps
    --pop-incr_coeff FLOAT          Increase in size of population per step
    --pop-incr_max INTEGER          Limit of new individuals added to the
                                    population per step
    ```

2. `from-jobs`: Run multiple jobs specified in provided CSV file.
    ```
    Usage: run_jobs.py [OPTIONS]

    Options:
    -p, --path TEXT  Path to the CSV file with jobs details  [required]
    ```

    The example of a file that can be passed as input can be found in `data/jobs.csv`.