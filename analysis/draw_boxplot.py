import os
import random
import glob
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import json

def draw_boxplots(path, step, required_properties, group_by_properties, outpath, cnt_per_category = None):
    groups, labels_spec = filter_files(path, required_properties, group_by_properties)
    
    if cnt_per_category is None:
        cnt_per_category = min([len(l) for l in list(groups.values())])

    data = []
    labels = []
    for i, group in enumerate(sorted(groups)):
        labels.append(group)
        cnt = []
        for directory in random.sample(groups[group], cnt_per_category):
            cnt.append(read_cnt_for_step(step, os.path.join(directory, "langs_per_step.csv")))
        data.append(cnt)

    # data = np.concatenate((std, mean, np.max(cnt), np.min(cnt)))

    fig1, ax1 = plt.subplots()
    ax1.set_title('For step=%s:\n%s' % (step, ','.join([" %s=%s" % (spec[0], spec[1]) for spec in required_properties])), fontsize=9)
    ax1.boxplot(data)
    plt.xticks([x+1 for x in range(len(labels))], labels, fontsize=9)
    plt.savefig(outpath)

def filter_files(path, required_properties, group_by_properties):
    groups = {}
    labels_spec = {}
    for directory in glob.glob(os.path.join(path, "*")):
        if os.path.exists(os.path.join(directory, "results.csv")):
            info = pd.read_csv(os.path.join(directory, "info.csv"), header=None, sep=';')
            cnt = sum([1 for row in info.values if (row[0], row[1].strip()) in required_properties])
            if cnt == len(required_properties):
                group_id_list = [row[1].strip()[:4] for row in info.values if row[0] in group_by_properties]
                group_id = '-'.join(group_id_list)
                if group_id not in groups:
                    groups[group_id] = []
                    labels = [row[0].strip() for row in info.values if row[0] in group_by_properties]
                    labels_spec[group_id] = list(zip(labels, group_id_list))
                groups[group_id].append(directory)
    return groups, labels_spec

def read_cnt_for_step(step, path):
    f = pd.read_csv(path, header=None, sep=';')
    steps = { row[0]: json.loads(row[1].strip().replace('\'', '\"')) for row in f.values}
    x = []; y = []; cnt = 0
    for s in sorted(steps):
        if step <= s:
            return cnt
        cnt += len(steps[s])
    return cnt

draw_boxplots("files/eval_select", 1000,
            [('init_pop_size', '30')],
            ['evaluate_type', 'select_type'],
            "/home/marta/Elka/ALHE/languages-evolution/analysis/graphs/boxplot_eval_select/pop_30_step_1000.png",
            20)