import os
import random
import glob
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import json

def draw_plot(path, required_properties, group_by_properties, outpath, cnt_per_category = None ):
    groups, labels_spec = filter_files(path, required_properties, group_by_properties)
    
    palette = ['#344966', '#E6AACE', '#87255B', '#2EBFA5', '#8C0B90', '#C0E4FF', '#27B502', '#7C60A8', '#CF95D7', '#145JKH']
    ax = plt.axes()

    if cnt_per_category is None:
        cnt_per_category = min([len(l) for l in list(groups.values())])

    for i, group in enumerate(sorted(groups)):
        print(group)
        for directory in random.sample(groups[group], cnt_per_category):
            print(directory)
            x, y = read_langs_per_step(os.path.join(directory, "langs_per_step.csv"))
            ax.plot(x, y, label=(','.join([" %s=%s" % (spec[0], spec[1]) for spec in labels_spec[group]])), color=palette[i])
            ax.scatter(x,y, color=palette[i])
    ax.set_title('Generated languages per step for:\n%s' % (','.join([" %s=%s" % (spec[0], spec[1]) for spec in required_properties])), fontsize=9)
    handles, labels = plt.gca().get_legend_handles_labels()
    newLabels, newHandles = [], []
    for handle, label in zip(handles, labels):
        if label not in newLabels:
            newLabels.append(label)
            newHandles.append(handle)
    plt.legend(newHandles, newLabels)
    plt.savefig(outpath)


def filter_files(path, required_properties, group_by_properties):
    groups = {}
    labels_spec = {}
    for directory in glob.glob(os.path.join(path, "*")):
        if os.path.exists(os.path.join(directory, "results.csv")):
            info = pd.read_csv(os.path.join(directory, "info.csv"), header=None, sep=';')
            cnt = sum([1 for row in info.values if (row[0], row[1].strip()) in required_properties])
            if cnt == len(required_properties):
                group_id_list = [row[1].strip() for row in info.values if row[0] in group_by_properties]
                group_id = '_'.join(group_id_list)
                if group_id not in groups:
                    groups[group_id] = []
                    labels = [row[0].strip() for row in info.values if row[0] in group_by_properties]
                    labels_spec[group_id] = list(zip(labels, group_id_list))
                groups[group_id].append(directory)
    return groups, labels_spec

def read_langs_per_step(path):
    f = pd.read_csv(path, header=None, sep=';')
    steps = { row[0]: json.loads(row[1].strip().replace('\'', '\"')) for row in f.values}
    x = []; y = []; cnt = 0
    for step in sorted(steps):
        x.append(step)
        cnt += len(steps[step])
        y.append(cnt)
    return x, y


# draw_plot("/mnt/langevol/langevol/eval_select_slow_growth",
#             [('init_pop_size', '100'), ('max_steps', '5000'), ('init_pop_type', 'GEO')],
#             ['evaluate_type', 'select_type'],
#             "graphs/eval_select_slow_growth/geo_100.png")

draw_plot("/mnt/langevol/langevol/init_pop_2",
            [('init_pop_size', '30'),('max_steps', '2000'),('evaluate_type','BINARY'), ('select_type', 'KEEP_EXISTING')],
            ['init_pop_type'],
            "graphs/init_pop/bin_keepex_30.png")
