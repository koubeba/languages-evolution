import os
from datetime import datetime
import pickle
import json

class StatsLogger:
    def __init__(self, base_path, init_pop, evaluate, select, pop_size, pmut_prob, pcross_prob, mut_prob, cross_prob, 
                max_steps, pop_incr_coeff, pop_incr_max):
        date = datetime.now()
        date_string = date.strftime("%Y%m%d%H%M%S")
        job_id = "%s_init_%s_eval_%s_sel_%s_popsize_%s_mpb_%.2f_pmpb_%.2f_cpb_%.2f_pcpb_%.2f_coeff_%.1f_maxinc_%s" % (date_string, 
                    init_pop[:3], evaluate[:3], select[:3], pop_size, mut_prob, pmut_prob, cross_prob, pcross_prob, pop_incr_coeff, pop_incr_max)
        self.dir_path = os.path.join(base_path, job_id)
        if not os.path.exists(self.dir_path):
            os.makedirs(self.dir_path)
        self.info = self.save_job_info(date, init_pop, evaluate, select, pop_size, pmut_prob, pcross_prob, mut_prob, cross_prob, max_steps, 
            pop_incr_coeff, pop_incr_max)
    
    def save_dict_to_file(self, pairs, file_name):
        with open(os.path.join(self.dir_path, file_name), "w") as f:
            for key in pairs:
                f.write("%s;%s\n" % (key, pairs[key]))

    def save_job_info(self, date, init_pop, evaluate, select, pop_size, pmut_prob, pcross_prob, mut_prob, cross_prob, max_steps, 
            pop_incr_coeff, pop_incr_max):
        pairs = { 
                "init_pop_type": init_pop,
                "evaluate_type": evaluate,
                "select_type": select,
                "init_pop_size": pop_size,
                "prop_mut_prob": pmut_prob,
                "mut_prob": mut_prob,
                "prop_cross_prob": pcross_prob,
                "cross_prob": cross_prob,
                "pop_incr_coeff": pop_incr_coeff,
                "pop_incr_max": pop_incr_max,
                "max_steps": max_steps,
                "date": date 
                }
        self.save_dict_to_file(pairs, "info.csv")
        return pairs

    def save_results(self, is_success, generated_language_cnt, loops_cnt):
        pairs = {
            "is_success": is_success,
            "generated_language_cnt": generated_language_cnt,
            "loops_cnt": loops_cnt
        }
        pairs.update(self.info)
        self.save_dict_to_file(pairs, "results.csv")

    def save_langs_pers_step(self, dictionary):
        with open(os.path.join(self.dir_path, "langs_per_step.csv"), "w") as f:
            for key, value in sorted(dictionary.items()):
                f.write("%s;%s\n" % (key, value))

    def dump_logbook(self, logbook):
        with open(os.path.join(self.dir_path, "logbook.pickle"), "wb") as f:
            pickle.dump(logbook, f)
        with open(os.path.join(self.dir_path, "logbook.txt"), "w") as f:
            f.write(str(logbook))

    def save_data_as_json(self, filename, data):
        with open(os.path.join(self.dir_path, ("{filename}.json").format(filename=filename)), "w") as f:
            json.dump(data, f)

    def save_langs_counter(self, dictionary):
        self.save_dict_to_file(dictionary, "all_langs_cnt.csv")