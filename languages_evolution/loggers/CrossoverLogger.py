import networkx as nx
from networkx.drawing.nx_agraph import write_dot, graphviz_layout
import matplotlib.pyplot as plt
from collections import defaultdict

from loggers.TreeLogger import TreeLogger

CROSSOVER_TYPE = "CROSSOVER"

class CrossoverLogger(TreeLogger):
    def add_crossover_child(self, parent1, parent2, child, step):
        """Add edges between parents and child language

        Arguments:
            parent1 {language}
            parent2 {language}
            child   {language}
            step    {int}       -- index of current evolution step
        """
        hashed_child = hash(str(child))
        self.graph.add_node(hashed_child)

        self.add_edge_to_parent(hash(str(parent1)), hashed_child, step, CROSSOVER_TYPE)
        self.add_edge_to_parent(hash(str(parent2)), hashed_child, step, CROSSOVER_TYPE)