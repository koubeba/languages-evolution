import networkx as nx

class TreeLogger:
    def __init__(self):
        self.graph = nx.DiGraph()


    def add_edge_to_parent(self, parent, child, step, edge_type):
        """Add edge to last existing parent

        Arguments:
            parent  {string} -- hashed parent
            child   {string} -- hashed child
            step    {int}    -- index of current evolution step
        """
        # If the parent doesn't exist, add the parent
        # Don't check the parent name- that could be a rare case where parent is generated in the same evolution step as the child
        # (for example in the first step)
        if not self.graph.has_node(parent):
            self.graph.add_node(parent)
            self.graph.add_edge(parent, child, step=step, type=edge_type)

        if self.name_assigned(parent):
            self.graph.add_edge(parent, child, step=step, type=edge_type)
        else:
            self.add_edge_to_precedessor(parent, child, step, edge_type)
    
    
    def add_edge_to_precedessor(self, parent, child, step, edge_type):
        predecessors = list(self.graph.predecessors(parent))
        if len(predecessors) != 0:
            self.graph.add_edge(predecessors[0], child, step=step, type= edge_type)
            self.graph.remove_edge(predecessors[0], parent)
            self.graph.remove_node(parent)
        else:
            self.graph.add_edge(parent, child, step=step, type=edge_type)
            

    def add_language_name(self, language, name):
        """Add name as node property

        Arguments:
            language {list of strings} -- list of properties
            name     {string}          -- language name
        """
        lang_hash = hash(str(language))
        self.graph.add_node(lang_hash, name=name) 


    def add_hash_language_name(self, lang_hash, name):
        """Add name as node property

        Arguments:
            language {int}      -- hash of language
            name     {string}   -- language name
        """
        self.graph.add_node(lang_hash, name=name) 


    def cleanup(self):
        """Removes nonexistent languages from the graph
        """
        for hashed_node in list(self.graph.nodes):
            if not self.name_assigned(hashed_node):
                self.graph.remove_node(hashed_node)

    
    def name_assigned(self, node_hash):
        """
        Arguments:
            node_hash {string} -- hash from the mode

        Returns:
            [bool] -- whether a name has been assigned to the language
        """
        if self.graph.has_node(node_hash):
            node = self.graph.nodes[node_hash]
            return 'name' in node.keys() and node['name'] != ""
        else:
            return False


    def substitute_mutated(self, lang, mutated_lang):
        """Update a node in the crossover graph if mutated

        Arguments:
            lang            {list of strings}
            mutated_lang    {list of strings}
        """
        old_lang = hash(str(lang))
        new_lang = hash(str(mutated_lang))

        self.graph.add_node(new_lang, name=self.graph.nodes[old_lang].get('name', ""))
        if self.graph.has_node(old_lang):
            # Substitute precedessors:
            for pre in list(self.graph.predecessors(old_lang)):
                edge_data = self.graph.get_edge_data(pre, old_lang)
                self.graph.add_edge(pre, new_lang, step = edge_data["step"], type=edge_data["type"])
                self.graph.remove_edge(pre, old_lang)
            for suc in list(self.graph.successors(old_lang)):
                edge_data = self.graph.get_edge_data(old_lang, suc)
                self.graph.add_edge(new_lang, suc, step = edge_data["step"], type=edge_data["type"])
                self.graph.remove_edge(old_lang, suc)
        self.graph.remove_node(old_lang)


    def node_link_format_data(self):
        self.cleanup()
        return nx.node_link_data(self.graph)