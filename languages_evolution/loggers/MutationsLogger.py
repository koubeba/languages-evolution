import networkx as nx
from collections import defaultdict

from loggers.TreeLogger import TreeLogger

MUTATIONS_TYPE = "MUTATION"

class MutationsLogger(TreeLogger):
    def add_mutation_child(self, parent, child, step):
        """Add edges between parents and child language

        Arguments:
            parent  {language}
            child   {language}
            step    {int}       -- index of current evolution step
        """
        hashed_child = hash(str(child))
        self.graph.add_node(hashed_child)

        self.add_edge_to_parent(hash(str(parent)), hashed_child, step, MUTATIONS_TYPE)