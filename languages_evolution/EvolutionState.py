class EvolutionState:
    def __init__(self):
        self.mutations = 0
        self.crossovers = 0
        self.steps = 0


    def addMutation(self):
        self.mutations = self.mutations+1

    
    def addCrossover(self):
        self.crossovers = self.crossovers+1

    
    def addStep(self):
        self.steps = self.steps+1