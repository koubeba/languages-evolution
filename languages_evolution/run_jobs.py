import click
import pandas as pd
import os
from run_cmd import run_cmd

def validate_path(ctx, param, value):
    if not os.path.exists(value):
        raise click.BadParameter("The given path does not exist")
    return value

@click.command()
@click.option('-p', '--path', callback=validate_path, required=True, help="Path to the CSV file with jobs details")
@click.option('-r', '--results-subdir', default="", help="Nested directory where results should be stored")
def from_file(path, results_subdir):
    """Run jobs from provided CSV file"""
    df = pd.read_csv(path)
    print("The following jobs have been read successfully:")
    print(df)

    log_tree_provided = 'log_tree' in df.columns
    kill_weak_offspring_provided = 'kill_weak_offspring' in df
    min100_provided = 'min_100' in df
    min500_provided = 'min500' in df
    min1000_provided = 'min1000' in df

    for ind, row in df.iterrows():
        print("Running job #%s..." %ind)
        for it in range(row['iterations']):
            print("Running iteration %s/%s..." % (it, row['iterations']))
            log_tree = df['log_tree'] if log_tree_provided else False
            run_cmd(init_pop = row['init_pop_type'],
                    evaluate = row['evaluate_type'],
                    select = row['select_type'],
                    pop_size = row['init_pop_size'],
                    pmut_prob = row['prop_mut_prob'],
                    pcross_prob = row['prop_cross_prob'],
                    mut_prob = row['mut_prob'],
                    cross_prob = row['cross_prob'],
                    max_steps = row['max_steps'],
                    pop_incr_coeff = row['pop_incr_coeff'],
                    pop_incr_max = row['pop_incr_max'],
                    log_to_console = False,
                    log_tree = log_tree,
                    kill_weak_offspring = row['kill_weak_offspring'] if kill_weak_offspring_provided else None,
                    min_100 = row['min_100'] if min100_provided else None,
                    min_500 = row['min_500'] if min500_provided else None,
                    min_1000 = row['min_1000'] if min1000_provided else None,
                    nested_dir=results_subdir)

if __name__ == '__main__':
    from_file()