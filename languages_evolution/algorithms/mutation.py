from deap import base
import random
from LangRepository import LangRepository


# indpb- independent probability for each attribute to be mutated
def basic_mutation(repository, indpb, individual):
    for index, attribute in enumerate(individual):
        if random.randint(0, 100)<(indpb*100):
            attribute = random.choice(repository.property_values[index])