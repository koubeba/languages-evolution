from math import sqrt
from geopy import distance


def dist_3d(p1, p2):
    return sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2+(p1[2]-p2[2])**2)


def get_shortest_dist_geographic(languages_coords, lang, selected_langs):
    point_coords = (languages_coords[lang][0], languages_coords[lang][1])
    points_coords = [(languages_coords[x][0], languages_coords[x][1]) for x in selected_langs]
    dists = [distance.distance(point_coords, coords) for coords in points_coords]
    if (len(dists) == 0):
        return 0
    return min(dists)


def get_shortest_dist_tsne_3d(languages_tsne_coords, lang, selected_langs):
    point_coords = (languages_tsne_coords[lang][0], languages_tsne_coords[lang][1], languages_tsne_coords[lang][2])
    points_coords = [(languages_tsne_coords[x][0], languages_tsne_coords[x][1], languages_tsne_coords[x][2]) for x in selected_langs]
    dists = [dist_3d(point_coords, coords) for coords in points_coords]
    if (len(dists) == 0):
        return 0
    return min(dists)