from deap import base, creator

def evaluate_zero(repository, log_tree, individual, step, console_logging_enabled = True, min_coincidence_rate = 1.0):
    """
        Dummy evaluate function: always returns 0
    """
    repository.check_languages_generation(individual, step, console_logging_enabled, min_coincidence_rate)
    return (0.0,)


def evaluate_binary(repository, log_tree, individual, step, console_logging_enabled = True, min_coincidence_rate = 1.0):
    """
        Returns 1 if language belongs to the repository; 0 otherwise
    """
    return (repository.check_binary_languages_generation(individual, step, console_logging_enabled, log_tree)[1],)


def evaluate_proportionately(repository, log_tree, individual, step, console_logging_enabled = True, min_coincidence_rate = 1.0):
    """
        Returns a coincidence rate between the given language 
        and the most similar existing language
    """
    return (repository.check_languages_generation(individual, step, console_logging_enabled, log_tree, min_coincidence_rate)[1],)
