from deap import tools

def sel_all_existing_and_tournament(individuals, k, tournsize, fit_attr="fitness"):
    """Select all the individuals with fitness=1 and, if the expected size
    of new generation *k* is greater than the obtained list, use the tournament
    selection algorithm to select the remaining part of the population. The list 
    returned contains references to the input *individuals*.
    
    We use the :func:`~selTournament` function provided by the DEAP library.
    It selects the best individual among *tournsize* randomly chosen
    individuals, *k* times. 
    
    :param individuals: A list of individuals to select from.
    :param k: The number of individuals to select.
    :param tournsize: The number of individuals participating in each tournament.
    :param fit_attr: The attribute of individuals to use as selection criterion
    :returns: A list of selected individuals.
    """

    chosen  = [ ind for ind in individuals if ind.fitness.values[0] == 1.0 ]
    if (len(chosen) >= k):
        return tools.selRandom(chosen, k)
    rest = [ ind for ind in individuals if ind.fitness.values[0] != 1.0 ]
    chosen.extend(tools.selTournament(rest, k - len(chosen), tournsize))
    # shuffle the list
    return tools.selRandom(chosen, k)