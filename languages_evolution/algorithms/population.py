import pandas
import random
from itertools import chain
from functools import partial
from deap import base, creator, tools
import numpy as np
from math import sqrt

from LangRepository import LangRepository
from algorithms.utils.geo_utils import get_shortest_dist_geographic, get_shortest_dist_tsne_3d


def init_random_population(repository, population_size, individual, toolbox = base.Toolbox()):
    """
        Registers the function initPopulation(pcls, ind_init) that can be registered in the creator
        The population consists of randomly chosen existing languages
    """
    random_language = partial(random.sample, repository.existing_languages.keys(), population_size)
    lang_names = tools.initIterate(list, random_language)

    init_population(repository, population_size, individual, toolbox, lang_names)


def init_population_from_clusters(repository, population_size, individual, toolbox = base.Toolbox()):
    """ 
        Registers the function initPopulation(pcls, ind_init) that can be registered in the creator
        The population consists of randomly chosen existing languages from all the clusters 
        and with uniform distribution across all clusters
    """
    def random_sample_clustered(languages_clustered, population_size, clusters_count):
        quotient, remainder = divmod(population_size, clusters_count)
        return chain.from_iterable([random.sample(languages_clustered[index], (quotient+(index<remainder))) for index in range(clusters_count)])
    
    random_language = partial(random_sample_clustered, repository.langs_clusters, population_size, repository.CLUSTERS)
    lang_names      = tools.initIterate(list, random_language)
    
    init_population(repository, population_size, individual, toolbox, lang_names)


def generate_init_popul(languages_coords, dist_function=get_shortest_dist_geographic, popul_size = 5, langs_selected_per_step = 5):
    available_langs = [ x for x in languages_coords ]
    selected_langs = []
    for step in range(popul_size):
        preselected_langs = random.sample(available_langs, langs_selected_per_step)
        shortest_dists = { lang: dist_function(languages_coords, lang, selected_langs) for lang in preselected_langs }
        max_dist_lang = max(shortest_dists, key=(lambda k: shortest_dists[k]))
        selected_langs.append(max_dist_lang)
        available_langs.remove(max_dist_lang)
    return selected_langs


def init_population_tsne(repository, population_size, individual, toolbox = base.Toolbox()):
    """
        Registers the function initPopulation(pcls, ind_init) that can be registered in the creator
        The population consists of randomly chosen existing languages. The coordinates of languages are
        taken into account when selecting the initial population to achieve roughly even geographical distribution.
    """
    generate_init_popul_func = partial(generate_init_popul, repository.langs_tsne_3d_coords, get_shortest_dist_tsne_3d, population_size)
    lang_names = tools.initIterate(list, generate_init_popul_func)
    init_population(repository, population_size, individual, toolbox, lang_names)


def init_population_geolocation(repository, population_size, individual, toolbox = base.Toolbox()):
    """
        Registers the function initPopulation(pcls, ind_init) that can be registered in the creator
        The population consists of randomly chosen existing languages. The TSNE coordinates of languages are
        taken into account when selecting the initial population to achieve a diverse population in terms of properties.
    """
    generate_init_popul_func = partial(generate_init_popul, repository.langs_geo_cords, get_shortest_dist_geographic, population_size)
    lang_names = tools.initIterate(list, generate_init_popul_func)
    init_population(repository, population_size, individual, toolbox, lang_names)


def init_population(repository, population_size, individual, toolbox, lang_names):
    def initPopulation(pcls, ind_init):
        return pcls(ind_init(repository.existing_languages[i]) for i in lang_names)

    toolbox.register("population_zero", initPopulation, list, individual)
    
    return initPopulation

