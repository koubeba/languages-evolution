import click
from enum import Enum
import numpy as np
import json
import math

from deap import base, creator, tools
import random
from functools import partial
from datetime import datetime

from LangRepository import LangRepository
from loggers.StatsLogger import StatsLogger

from algorithms.population import init_random_population, init_population_geolocation, init_population_tsne, init_population_from_clusters
from algorithms.evaluate import evaluate_binary, evaluate_zero, evaluate_proportionately
from algorithms.select import sel_all_existing_and_tournament
from algorithms.mutation import basic_mutation

class InitiatePopTypes(Enum):
    RANDOM  = 'RANDOM'
    GEO     = 'GEO'
    TSNE    = 'TSNE'
    CLUSTER = 'CLUSTER'


class EvaluateTypes(Enum):
    ZERO            = 'ZERO'
    BINARY          = 'BINARY'
    PROPORTIONAL    = 'PROPORTIONAL'

class SelectTypes(Enum):
    KEEP_EXISTING   = 'KEEP_EXISTING' # if a generated language exists, it will always be selected
    TOURNAMENT      = 'TOURNAMENT'

@click.command()
# arguments specifying type of algorithms
@click.option('--init-pop', type=click.Choice([t.value for t in InitiatePopTypes], case_sensitive=False), default=InitiatePopTypes.RANDOM.value, 
                help="Type of algorithm to generate initial population")
@click.option('--evaluate', type=click.Choice([t.value for t in EvaluateTypes], case_sensitive=False), default=EvaluateTypes.BINARY.value,
                help="Type of algorithm to calculate fitness")
@click.option('--select', type=click.Choice([t.value for t in SelectTypes], case_sensitive=False), default=SelectTypes.KEEP_EXISTING.value,
                help="Type of algorithm to select individuals to the next generation")
# parameters
@click.option('--pop-size', type=int, default=30, help='Size of initial population')
@click.option('--pmut-prob', default=0.2, help='Independent probability of one property mutation')
@click.option('--pcross-prob', default=0.5, help='Independent probability of crossing a property between mating individuals')
@click.option('--mut-prob', default=0.2, help='Independent probability of mutation between a pair of individuals')
@click.option('--cross-prob', default=0.5, help='Independent probability of crossing a pair of individuals')
@click.option('--max-steps', default=10000, help='Maximal number of evolution steps')
@click.option('--pop-incr-coeff', default=1.0, help="Increase in size of population per step")
@click.option('--pop-incr-max', type=int, help="Limit of new individuals added to the population per step (can be None)")
@click.option('--kill-weak-offspring', type=float, help="Percentage of offspring that will make it to next evolution step")
@click.option('--log-to-console', type=bool, default=True)
@click.option('--log-tree', type=bool, default=False)
@click.option('--min-100', type=int, help="Stop evolving if didn't generate X languages till 100th step")
@click.option('--min-500', type=int, help="Stop evolving if didn't generate X languages till 500th step")
@click.option('--min-1000', type=int, help="Stop evolving if didn't generate X languages till 1000th step")
def main(init_pop, evaluate, select, pop_size, pmut_prob, pcross_prob, mut_prob, cross_prob, max_steps, 
            pop_incr_coeff, pop_incr_max, log_to_console, log_tree, kill_weak_offspring, min_100, min_500, min_1000):
    run_cmd(init_pop, evaluate, select, pop_size, pmut_prob, pcross_prob, mut_prob, cross_prob, max_steps, 
            pop_incr_coeff, pop_incr_max, log_to_console, log_tree, kill_weak_offspring, min_100, min_500, min_1000)

def run_cmd(init_pop, evaluate, select, pop_size, pmut_prob, pcross_prob, mut_prob, cross_prob, max_steps, 
            pop_incr_coeff, pop_incr_max, log_to_console, log_tree, kill_weak_offspring = None, min_100 = None, min_500 = None, min_1000 = None, nested_dir = ""):
    
    random.seed(datetime.now())
    base_path = "results"
    if (nested_dir != ""):
        base_path += ("/%s" % nested_dir)
    repository = LangRepository()

    # Prepare tools for statistics logging
    stats_logger = StatsLogger(base_path, init_pop, evaluate, select, pop_size, pmut_prob, pcross_prob, mut_prob, 
                                cross_prob, max_steps, pop_incr_coeff, pop_incr_max)
    stats = tools.Statistics(key=lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    stats.register("cnt_max", lambda vec: sum([1 for el in vec if el[0] == 1.0]))
    stats.register("gen_cnt", lambda vec: repository.languages_generated_cnt())
    stats.register("size", len)

    logbook = tools.Logbook()
    logbook.header = "gen", "avg", "std", "min", "max", "cnt_max", "gen_cnt", "size"
    
    # Prepare individuals
    toolbox = base.Toolbox()

    creator.create("fitness_max", base.Fitness, weights=(1.0,))
    creator.create("individual_zero", list, fitness=creator.fitness_max)

    # Choose type of algorithm to generate starting population
    if (init_pop == InitiatePopTypes.RANDOM.value):
        init_random_population(repository, pop_size, creator.individual_zero, toolbox)
    elif (init_pop == InitiatePopTypes.GEO.value):
        init_population_geolocation(repository, pop_size, creator.individual_zero, toolbox)
    elif (init_pop == InitiatePopTypes.TSNE.value):
        init_population_tsne(repository, pop_size, creator.individual_zero, toolbox)
    elif (init_pop == InitiatePopTypes.CLUSTER.value):
        init_population_from_clusters(repository, pop_size, creator.individual_zero, toolbox)
    else:
        print("Unknown InitiatePopTypes type %s. Aborting..." % init_pop.value)

    # Choose type of algorithm for evaluate function
    if (evaluate == EvaluateTypes.ZERO.value):
        toolbox.register("evaluate", evaluate_zero, repository, log_tree)
    elif (evaluate == EvaluateTypes.BINARY.value):
        toolbox.register("evaluate", evaluate_binary, repository, log_tree)
    elif (evaluate == EvaluateTypes.PROPORTIONAL.value):
        toolbox.register("evaluate", evaluate_proportionately, repository, log_tree)
    else:
        print("Unknown EvaluateTypes type %s. Aborting..." % evaluate.value)

    # Choose type of algorithm for selection function
    if (select == SelectTypes.KEEP_EXISTING.value):
        toolbox.register("select", sel_all_existing_and_tournament, tournsize=5)
    elif (select == SelectTypes.TOURNAMENT.value):
        toolbox.register("select", tools.selTournament, tournsize=5)
    else:
        print("Unknown SelectTypes type %s. Aborting..." % select.value)

    toolbox.register("mate", tools.cxUniform)
    toolbox.register("mutate", basic_mutation, repository, pmut_prob)

    population = toolbox.population_zero()
    
    # Evaluate the entire population
    fitnesses = list(map(partial(toolbox.evaluate, step=0, console_logging_enabled=log_to_console), population))
    for ind, fit in zip(population, fitnesses):
        ind.fitness.values = fit

    def are_indentical(first, second):
        return all([prop1 == prop2 for prop1, prop2 in zip(first, second)])
    
    # Function performing a single iteration
    def evolution_step(step):
        # Generate stats for population
        record = stats.compile(population)
        logbook.record(gen=step, **record)

        # Select the next generation individuals
        offspring = []
        if (pop_incr_max == None or math.isnan(pop_incr_max)):
            offspring = toolbox.select(individuals = population, k = math.ceil(pop_incr_coeff * len(population)))
        else:
            offspring = toolbox.select(individuals = population, k = min(math.ceil(pop_incr_coeff * len(population)), pop_incr_max))
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))

        # Apply crossover and mutation
        for first, second in zip(offspring[::2], offspring[1::2]):
            first_copy = first.copy(); second_copy = second.copy()
            if random.random() < cross_prob:
                toolbox.mate(first, second, pcross_prob)
                if log_tree:
                    repository.add_crossover_child_to_logger(first_copy, second_copy, first, step)
                    repository.add_crossover_child_to_logger(first_copy, second_copy, second, step)

            if random.random() < mut_prob:
                toolbox.mutate(first)
            
            if random.random() < mut_prob:
                toolbox.mutate(second)
            
            if not are_indentical(first_copy, first):
                del first.fitness.values
                if log_tree:
                    repository.add_mutation_child_to_logger(first_copy, first, step)
                    repository.substitute_mutant(first_copy, first)

            if not are_indentical(second_copy, second):
                del second.fitness.values
                if log_tree:
                    repository.add_mutation_child_to_logger(second_copy, second, step)
                    repository.substitute_mutant(second_copy, second)


        # Add new individuals to the population
        new_inds = [ ind for ind in offspring if not ind.fitness.valid ]
        fitnesses = list(map(partial(toolbox.evaluate, step=step, console_logging_enabled=log_to_console), new_inds))
        for ind, fit in zip(new_inds, fitnesses):
            ind.fitness.values = fit

        # select only best children
        if (select == SelectTypes.KEEP_EXISTING.value and evaluate == EvaluateTypes.PROPORTIONAL.value and 
                kill_weak_offspring is not None):
            new_inds =  toolbox.select(individuals = new_inds, k = math.ceil(kill_weak_offspring * len(new_inds)))

        population.extend(new_inds)

    # Variable keeping track on the number of iterations
    step = 0

    while (not repository.all_generated()) and step < max_steps:
        if log_to_console:
            print(("\tEvolution step: {step}, population size: {size}").format(step = step, size = len(population)))
        elif step % 500 == 0:
            print(("{date}: \tEvolution step: {step}, population size: {size}").format(date = datetime.now(), step = step, size = len(population)))
        if (step==100 and min_100 is not None):
            if repository.languages_generated_cnt() < min_100:
                print("Only %s langs generated at 100th step. Finiship up attempt..." % repository.languages_generated_cnt())
                break
        if (step==500 and min_500 is not None):
            if repository.languages_generated_cnt() < min_500:
                print("Only %s langs generated at 500th step. Finiship up attempt..." % repository.languages_generated_cnt())
                break
        if (step==1000 and min_1000 is not None):
            if repository.languages_generated_cnt() < min_1000:
                print("Only %s langs generated at 1000th step. Finiship up attempt..." % repository.languages_generated_cnt())
                break
        evolution_step(step)
        step += 1
    if log_to_console:
        print("FINISHED")
        print(("Population size {population}".format(population = pop_size)))
        print(("Mutation probability: {mut_prob}").format(mut_prob = pmut_prob))
        print(("Crossover probability: {crossover_prob}").format(crossover_prob = pcross_prob))
        print(("Finished with {generated} languages").format(generated = repository.languages_generated_cnt()))

    # Save results
    if log_tree:
        crossover_data, mutations_data = repository.get_node_link_data()
        stats_logger.save_data_as_json("node_link_crossover", crossover_data)
        stats_logger.save_data_as_json("node_link_mutation", mutations_data)

    stats_logger.save_results(repository.all_generated(), repository.languages_generated_cnt(), step)
    stats_logger.save_langs_pers_step(repository.langs_per_step)
    stats_logger.dump_logbook(logbook)
    stats_logger.save_langs_counter(repository.was_generated)

if __name__ == '__main__':
    main()