import pandas as pd
import numpy as np

from loggers.CrossoverLogger import CrossoverLogger
from loggers.MutationsLogger import MutationsLogger

class LangRepository:
    def __init__(self):

        # Existing languages is a dict of form  language_name: list of language properties
        language_info = pd.read_csv('data/language_info.csv')
        language_codes = list(language_info['wals_code'])
        features = pd.read_csv('data/encoded_features_int.csv', header=None)

        self.existing_languages = {name: list(values) for name, values in zip(language_codes, features.values)}
        self.CLUSTERS = language_info['cluster'].max()+1

        # Additional dicts with properties used by init population generation functions
        self.langs_geo_cords      = {name: list(values) for name, values in zip(language_codes, language_info[['latitude', 'longitude']].values)}
        self.langs_tsne_3d_coords = {name: list(values) for name, values in zip(language_codes, language_info[['tsne_x', 'tsne_y', 'tsne_z']].values)}
        self.langs_clusters       = [list(language_info.loc[language_info['cluster']==index]['wals_code'].values) for index in range(self.CLUSTERS)]

        # Property values is an array of possible values per column; it's used to mutate individuals
        self.property_values = [list(features[col].unique()) for col in list(features)]

        # List with calculated language properties hashes
        self.lang_hashes = {name: hash(str(list(values))) for name, values in zip(language_codes, self.existing_languages.values())}

        # Generated langues is a dict of form  language_name: number of times it has been generated
        self.was_generated = {name: 0 for name in language_codes}

        # Languages generated through the process
        self.left_to_generate = list(self.was_generated.keys())

        # Dictionary mapping step number to the list of languages generated within it
        self.langs_per_step = { }

        # Helper constant values
        self.LANGUAGE_COUNT = len(language_codes)

        self.mutations_logger = MutationsLogger()
        self.crossover_logger = CrossoverLogger()

    def calculate_coincidence_rate(self, existing_language_name, generated_language):
        """Checks what is a coincidence rate between given existing and generated languages 
        
        Arguments:
            existing_language_name  {string} -- name of the language to test
            generated_language      {list of strings} -- generated language in form of list of property values

        Returns:
            float -- properties coincidence rate
        """
        language = self.existing_languages[existing_language_name]
        equal_properties = sum([1 for prop1, prop2 in zip(language, generated_language) if prop1 == prop2])
        return (equal_properties / len(language))


    def check_languages_generation(self, generated_language, step, console_logging_enabled, log_tree_enabled, min_coincidence_rate):
        """Checks whether any of existing languages has been generated

        Arguments:
            generated_language      {list of strings} -- generated language in form of 
                                                         list of property values
            percentage_properties   {real number} -- percentage of properties needed to match 
                                                     to consider language generated
            step                    {int} -- number of the current step; 
                                             used to save information in which steps languages were generated
        Returns:
            strings -- name of the generated language, empty string if no generated language
            float   -- properties coincidence rate of the most similar language
        """
        currently_best = None; currently_best_coincidence_rate = 0.0
        for lang_name in self.left_to_generate:
            coincidence_rate = self.calculate_coincidence_rate(lang_name, generated_language)
            if coincidence_rate > currently_best_coincidence_rate:
                currently_best = lang_name; currently_best_coincidence_rate = coincidence_rate
        if currently_best_coincidence_rate >= min_coincidence_rate:
            self.was_generated[currently_best] += 1
            # Delete the duplicates- the duplicates lead to explosion
            if currently_best in self.left_to_generate:
                self.mark_language_generated(currently_best, step, console_logging_enabled)
                if log_tree_enabled:
                    self.crossover_logger.add_language_name(self.existing_languages[currently_best], currently_best)
                    self.mutations_logger.add_language_name(self.existing_languages[currently_best], currently_best)
                return currently_best, currently_best_coincidence_rate
        return "", currently_best_coincidence_rate


    def check_binary_languages_generation(self, generated_language, step, console_logging_enabled, log_tree_enabled):
        """Checks if the generate language has all of properties identical to some language

        Arguments:
            generated_language      {list of strings}   -- generated language in form of 
                                                           list of property values
            step                    {int}               -- number of the current step; 
                                                           used to save information in which steps languages were generated
            console_logging_enabled {bool}
        """
        generated_hash = hash(str(generated_language))
        for lang_name in self.lang_hashes:
            if generated_hash == self.lang_hashes[lang_name]:
                self.was_generated[lang_name] += 1
                if log_tree_enabled:
                    self.crossover_logger.add_hash_language_name(generated_hash, lang_name)
                    self.mutations_logger.add_hash_language_name(generated_hash, lang_name)
                # We want to return 1.0 only if the language was generated for the first time
                if self.was_generated[lang_name] == 1:
                    self.mark_language_generated(lang_name, step, console_logging_enabled)
                    return lang_name, 1.0
        return "", 0.0



    def mark_language_generated(self, language_name, step, console_logging_enabled):
        """Mark language as generated and append to list of languages generated in this step

        Arguments:
            language_name           {string}
            step                    {int}               -- number of the current step; 
                                                           used to save information in which steps languages were generated
            console_logging_enabled {bool}              -- [description]
        """
        if console_logging_enabled:
            print(("Generated the {lang_name} language!").format(lang_name=language_name))
        if step not in self.langs_per_step:
            self.langs_per_step[step] = []
        self.left_to_generate.remove(language_name)
        self.langs_per_step[step].append(language_name)


    def languages_generated_cnt(self):
        """
        Returns:
            int -- number of generated languages
        """
        return sum([times_generated>=1 for times_generated in self.was_generated.values()])


    def all_generated(self):
        """
        Returns:
            bool -- whether all languages have been generated
        """
        return self.languages_generated_cnt() == self.LANGUAGE_COUNT

    
    def add_crossover_child_to_logger(self, parent1, parent2, child, step):
        """Add child to the crossover logger

        Arguments:
            parent1 {list of strings}
            parent2 {list of strings}
            child   {list of strings}
            step    {int}               -- step of evolution
        """
        self.crossover_logger.add_crossover_child(parent1, parent2, child, step)
    
    
    def add_mutation_child_to_logger(self, parent, child, step):
        """Add child to the mutation logger

        Arguments:
            parent  {list of strings}
            child   {list of strings}
            step    {int}               -- step of evolution
        """
        self.mutations_logger.add_mutation_child(parent, child, step)


    def substitute_mutant(self, old, new):
        """If a language mutated, substitute in crossover graph

        Arguments:
            old {list of strings}
            new {list of strings}
        """
        self.crossover_logger.substitute_mutated(old, new)


    def get_node_link_data(self):
        return (self.crossover_logger.node_link_format_data(), self.mutations_logger.node_link_format_data())