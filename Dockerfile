FROM python:3

WORKDIR /usr/local/app

COPY languages_evolution/ .

RUN pip install -r ./requirements.txt